#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/uio.h>
#include <fcntl.h>
#include <errno.h>

int build_socket(void);
int connect_socket(void);
void socks4(int);

int main()
{
  int sockfd, listenfd = build_socket();
  struct sockaddr_in cli_addr;
  socklen_t clilen;
	clilen = sizeof(cli_addr);

  while(1) {
    char env[100];
    int pid;

    if( (sockfd = accept(listenfd, (struct sockaddr *) &cli_addr, &clilen)) < 0) exit(3);
    printf("== accept ==\n");
    sprintf(env, "%s", inet_ntoa(cli_addr.sin_addr));
    printf("Source Addr = %s(%u) \n", inet_ntoa(cli_addr.sin_addr), cli_addr.sin_port);
    setenv("REMOTE_ADDR", env, 1);
    setenv("REMOTE_HOST", env, 1);
    if( (pid = fork()) < 0)
      exit(3);
    else if(pid == 0) {
      printf("== fork child ==\n");
      close(listenfd);
      socks4(sockfd);
      close(sockfd);
      printf("== end child ==\n");
      return 0;
    }
    close(sockfd);
  }
}

int build_socket(void)
{
	int sockfd, port;
  int binding = 0;
	struct sockaddr_in serv_addr;


  sockfd =  socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0)
      printf("when opening socket... \n");
	bzero ((char *) &serv_addr, sizeof(serv_addr));

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;

  do {
    if(binding) printf("when binding... \n");
  	printf("Enter port: ");
  	scanf("%d", &port);
    serv_addr.sin_port = htons(port);
  } while( binding = bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0);

    if(listen(sockfd,128) < 0) exit(3);

    printf("== proxy start ==\n");
    return sockfd;
}

/*int connect_socket(void)
{
  int sock;
  struct sockaddr_in web_addr;
  sock = socket(AF_INET,SOCK_STREAM,0);
  memset(&web_addr, 0, sizeof(web_addr));
  web_addr.sin_family = AF_INET;
  web_addr.sin_addr.s_addr = IP_TMP;
  web_addr.sin_port = htons(PORT_TMP);
  if( connect(sock, (struct sockaddr *)&web_addr, sizeof(web_addr)) < 0) return -1;
  return sock;
}*/

void socks4(int browser_fd)
{
  printf("== socks ==\n");
  unsigned char request[100], reply[8];
  read(browser_fd, request,sizeof(request));
  //printf("== %s ==\n", request);
  unsigned char VN;
  VN = request[0];
  unsigned char CD;
  CD = request[1];
  unsigned int DST_PORT;
  DST_PORT = request[2] << 8 | request[3];
  unsigned int DST_IP;
  DST_IP = request[7] << 24 | request[6] << 16 | request[5] << 8 | request[4];

  char* USER_ID = (char*)request + 8;

  if(VN != 0x04) exit(0);


  unsigned char address[4];
  address[0] = (unsigned char)atoi("140");
  address[1] = (unsigned char)atoi("113");
  address[2] = (unsigned char)atoi("0");
  address[3] = (unsigned char)atoi("0");
  if(CD == 0x02) reply[1] = 0x5A;
  if(CD == 0x01) {
    /*if( ( (address[0] == request[4]) || (address[0] == 0x00)) && (address[1] == request[5]) || (address[1] == 0x00))
    && (address[2] == request[6]) || (address[2] == 0x00)) && (address[3] == request[7]) || (address[3] == 0x00)))*/ reply[1] = 0x5A;
  }

  printf("------------request-----------\n");
  printf("Mode = %s USER_ID=%s\n",(CD==0x01)?"CONNECT":"BIND", USER_ID);
  printf("Destination Addr = %u.%u.%u.%u\n", request[4], request[5], request[6], request[7]);
  printf("Destination Port = %u\n", DST_PORT);
  printf("------------------------------\n");

  if(CD ==0x01) { //connect mode
    int dst_fd = 0;
    printf("== connect mode ==\n");
    reply[0] = 0;
    reply[2] = request[2];
    reply[3] = request[3];
    reply[4] = request[4];
    reply[5] = request[5];
    reply[6] = request[6];
    reply[7] = request[7];
    write(browser_fd, reply, 8);
    printf("== reply ==\n");

    struct sockaddr_in web_addr;
    dst_fd = socket(AF_INET,SOCK_STREAM,0);
    memset(&web_addr, 0, sizeof(web_addr));
    web_addr.sin_family = AF_INET;
    web_addr.sin_addr.s_addr = DST_IP;
    web_addr.sin_port = htons(DST_PORT);
    if( connect(dst_fd, (struct sockaddr *)&web_addr, sizeof(web_addr)) < 0){
      printf("== connect fail ==\n");
      return;
    }
    if(dst_fd == 0) return;

    fd_set rfds, afds;
    int nfds;
    nfds = ((dst_fd > browser_fd)?dst_fd:browser_fd) + 1;
    FD_ZERO(&rfds);
    FD_ZERO(&afds);
    FD_SET(browser_fd, &afds);
    FD_SET(dst_fd, &afds);

    char buffer1[15000], buffer2[15000];
    int cmd, rst, browser_end = 0, dst_end = 0;
    while(1) {
      memcpy(&rfds, &afds, sizeof(rfds));
      if(select(nfds, &rfds, NULL, NULL, NULL) < 0) {
        printf("select error\n");
        exit(1);
      }

      if(FD_ISSET(browser_fd, &rfds)) {
        memset(buffer1, '\0', sizeof(buffer1));
        cmd = read(browser_fd, buffer1, sizeof(buffer1));
        if(cmd == 0) {
          FD_CLR(browser_fd, &afds);
          close(browser_fd);
          browser_end = 1;
          printf("== browser end ==\n");
        } else if(cmd == -1) {
          FD_CLR(browser_fd, &afds);
          close(browser_fd);
          browser_end = 1;
          printf("== browser error ==\n");
        } else write(dst_fd, buffer1, cmd);
      }

      if(FD_ISSET(dst_fd, &rfds)) {
        memset(buffer2, '\0', sizeof(buffer2));
        rst = read(dst_fd, buffer2, sizeof(buffer2));
        if(rst == 0) {
          FD_CLR(dst_fd, &afds);
          close(dst_fd);
          dst_end = 1;
          printf("== server end ==\n");
        } else if(rst == -1) {
          FD_CLR(dst_fd, &afds);
          close(dst_fd);
          dst_end = 1;
          printf("== server error ==\n");
        }else write(browser_fd, buffer2, rst);
        if(dst_end) {
          FD_CLR(browser_fd, &afds);
          close(browser_fd);
          browser_end = 1;
          printf("== browser closed ==\n");
        }
      }

      if(browser_end == 1 && dst_end == 1) {
        break;
      }
    } //end while
  } //connect mode
  else if(CD ==0x02) { //bind mode
    printf("== bind mode ==\n");
    int bind_fd;
    static struct sockaddr_in bind_addr;

    if( (bind_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) exit(3);
    bind_addr.sin_family = AF_INET;
    bind_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    bind_addr.sin_port = htons(INADDR_ANY);
    if(bind(bind_fd, (struct sockaddr *)&bind_addr, sizeof(bind_addr)) < 0) {
      printf("bind fail\n");
      exit(3);
    }

    int n, serAlc_len;
    struct sockaddr_in serAlc;
    serAlc_len = sizeof serAlc;
    n = getsockname(bind_fd, (struct sockaddr *)&serAlc, (socklen_t *)&serAlc_len);
    if( n < 0) {
      printf("== getsockname fail ==\n");
      sleep(2);
    }
    if(listen(bind_fd, 5) < 0) exit(3);

    reply[0] = 0;
    reply[2] = (unsigned char)(ntohs(serAlc.sin_port)/256);
    reply[3] = (unsigned char)(ntohs(serAlc.sin_port)%256);
    reply[4] = 0;
    reply[5] = 0;
    reply[6] = 0;
    reply[7] = 0;
    write(browser_fd, reply, 8);
    printf("== reply 1 ==\n");
    printf("%u\n", serAlc.sin_port);

    int ftp_fd, length;
    struct sockaddr_in ftp_addr;
    length = sizeof(ftp_addr);
    if( (ftp_fd = accept(bind_fd, (struct sockaddr *)&ftp_addr, (socklen_t*)&length)) < 0) {
      printf("FTP fail\n");
      exit(3);
    }
    printf("%u\n", serAlc.sin_port);
    printf("FTP: %d\n", ftp_fd);

    write(browser_fd, reply, 8);
    printf("== reply 2 ==\n");

    fd_set rfds, afds;
    int nfds;
    nfds = ((ftp_fd > browser_fd)?ftp_fd:browser_fd) + 1;
    FD_ZERO(&rfds);
    FD_ZERO(&afds);
    FD_SET(browser_fd, &afds);
    FD_SET(ftp_fd, &afds);

    char buffer1[1024], buffer2[1024];
    int cmd, rst, browser_end = 0, ftp_end = 0;
    while(1) {
      memcpy(&rfds, &afds, sizeof(rfds));
      if(select(nfds, &rfds, NULL, NULL, NULL) < 0) {
        printf("select error\n");
        exit(1);
      }

      if(FD_ISSET(browser_fd, &rfds)) {
        memset(buffer1, '\0', sizeof(buffer1));
        cmd = read(browser_fd, buffer1, sizeof(buffer1));
        if(cmd <= 0) {
          FD_CLR(browser_fd, &afds);
          close(browser_fd);
          browser_end = 1;
        } else write(ftp_fd, buffer1, cmd);
      }

      if(FD_ISSET(ftp_fd, &rfds)) {
        memset(buffer2, '\0', sizeof(buffer2));
        rst = read(ftp_fd, buffer2, sizeof(buffer2));
        if(rst <= 0) {
          FD_CLR(ftp_fd, &afds);
          close(ftp_fd);
          ftp_end = 1;
        } else write(browser_fd, buffer2, rst);
      }

      if(browser_end == 1 && ftp_end == 1) {
        break;
      }
    } //end while
  } //bind mode
  printf("== end socks ==\n");
  return;
}
